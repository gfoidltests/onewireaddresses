// OneWireUtils.h

#ifndef _ONEWIREUTILS_h
#define _ONEWIREUTILS_h

#if defined(ARDUINO) && ARDUINO >= 100
	#include "arduino.h"
#else
	#include "WProgram.h"
#endif

const byte oneWirePin = 7;

void disoverOneWireDevices();

#endif