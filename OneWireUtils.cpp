#include <OneWire.h>
#include "OneWireUtils.h"

void disoverOneWireDevices()
{
	OneWire ds(oneWirePin);
	byte addr[8];

	Serial.println(F("-----------------------------"));
	Serial.println(F("Discovering 1-Wire devices..."));
	Serial.println();

	ds.reset_search();
	while (ds.search(addr))
	{
		Serial.println(F("\nFound \'1-Wire\' device"));

		Serial.print(F("Address: "));
		for (int i = 0; i < 8; ++i)
		{
			Serial.print(F("0x"));
			
			if (addr[i] < 16) 
				Serial.print(F("0"));

			Serial.print(addr[i], HEX);

			if (i < 7)
				Serial.print(F(" "));
		}
		Serial.println();

		if (OneWire::crc8(addr, 7) != addr[7])
		{
			Serial.println(F("CRC is not valid!"));
			return;
		}

		if (addr[0] == 0x10)
			Serial.println(F("Device is a DS18S20 family device."));
		else if (addr[0] == 0x28)
			Serial.println(F("Device is a DS18B20 family device."));
		else
		{
			Serial.println(F("Device family is not recognized: 0x"));
			Serial.print(addr[0], HEX);
			return;
		}

		Serial.println();
	}

	Serial.println(F("\nDiscovery done."));
	ds.reset_search();
}